"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampsoftcrud_repository_base_1 = require("./dto/timestampsoftcrud.repository.base");
let OrderRepository = class OrderRepository extends timestampsoftcrud_repository_base_1.TimestampSoftCrudRepository {
    constructor(dataSource, paymentRepositoryGetter) {
        super(models_1.Order, dataSource);
        this.paymentRepositoryGetter = paymentRepositoryGetter;
        this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter);
        this.registerInclusionResolver('payments', this.payments.inclusionResolver);
    }
};
OrderRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.ordrinadb')),
    tslib_1.__param(1, repository_1.repository.getter('PaymentRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.OrdrinadbDataSource, Function])
], OrderRepository);
exports.OrderRepository = OrderRepository;
//# sourceMappingURL=order.repository.js.map