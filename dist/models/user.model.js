"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const loopback4_soft_delete_1 = require("loopback4-soft-delete");
const enum_1 = require("../types/enum");
const company_model_1 = require("./company.model");
const order_model_1 = require("./order.model");
const payment_model_1 = require("./payment.model");
const product_model_1 = require("./product.model");
const user_credential_model_1 = require("./user-credential.model");
let User = class User extends loopback4_soft_delete_1.SoftDeleteEntity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        id: true,
        generated: true,
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "firstName", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "lastName", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "email", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        jsonSchema: {
            enum: Object.values(enum_1.USER_ROLE),
        },
        required: false,
        default: enum_1.USER_ROLE.customer,
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "role", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "phone", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "address", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], User.prototype, "avatar", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date',
        default: () => new Date(),
    }),
    tslib_1.__metadata("design:type", Date)
], User.prototype, "createdAt", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date',
        default: () => new Date(),
    }),
    tslib_1.__metadata("design:type", Date)
], User.prototype, "updatedAt", void 0);
tslib_1.__decorate([
    (0, repository_1.hasOne)(() => user_credential_model_1.UserCredential),
    tslib_1.__metadata("design:type", user_credential_model_1.UserCredential)
], User.prototype, "userCredentials", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => order_model_1.Order, { keyTo: 'customerId' }),
    tslib_1.__metadata("design:type", Array)
], User.prototype, "orders", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => payment_model_1.Payment, { keyTo: 'customerId' }),
    tslib_1.__metadata("design:type", Array)
], User.prototype, "payments", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => company_model_1.Company),
    tslib_1.__metadata("design:type", Array)
], User.prototype, "companies", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => product_model_1.Product),
    tslib_1.__metadata("design:type", Array)
], User.prototype, "products", void 0);
User = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], User);
exports.User = User;
//# sourceMappingURL=user.model.js.map