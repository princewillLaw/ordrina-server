import { CompanyRepository, OrderRepository, PaymentRepository, ProductRepository, UserRepository } from '../repositories';
export declare class DeleteController {
    protected userRepository: UserRepository;
    protected companyRepository: CompanyRepository;
    protected productRepository: ProductRepository;
    protected orderRepository: OrderRepository;
    protected paymentRepository: PaymentRepository;
    constructor(userRepository: UserRepository, companyRepository: CompanyRepository, productRepository: ProductRepository, orderRepository: OrderRepository, paymentRepository: PaymentRepository);
    deleteProductById(userId: string, productId: string): Promise<void>;
    deleteOrderById(userId: string, orderId: string): Promise<void>;
    deletePaymentById(userId: string, paymentId: string): Promise<void>;
    deleteCompanyById(userId: string, companyId: string): Promise<void>;
}
