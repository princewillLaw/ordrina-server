import { Getter } from '@loopback/core';
import { HasManyRepositoryFactory, HasOneRepositoryFactory } from '@loopback/repository';
import { OrdrinadbDataSource } from '../datasources';
import { Company, Order, Payment, Product, User, UserCredential, UserRelations } from '../models';
import { CompanyRepository } from './company.repository';
import { TimestampSoftCrudRepository } from './dto/timestampsoftcrud.repository.base';
import { OrderRepository } from './order.repository';
import { PaymentRepository } from './payment.repository';
import { ProductRepository } from './product.repository';
import { UserCredentialRepository } from './user-credential.repository';
export declare class UserRepository extends TimestampSoftCrudRepository<User, typeof User.prototype.id, UserRelations> {
    protected userCredentialRepositoryGetter: Getter<UserCredentialRepository>;
    protected orderRepositoryGetter: Getter<OrderRepository>;
    protected paymentRepositoryGetter: Getter<PaymentRepository>;
    protected companyRepositoryGetter: Getter<CompanyRepository>;
    protected productRepositoryGetter: Getter<ProductRepository>;
    readonly userCredentials: HasOneRepositoryFactory<UserCredential, typeof User.prototype.id>;
    readonly orders: HasManyRepositoryFactory<Order, typeof User.prototype.id>;
    readonly payments: HasManyRepositoryFactory<Payment, typeof User.prototype.id>;
    readonly companies: HasManyRepositoryFactory<Company, typeof User.prototype.id>;
    readonly products: HasManyRepositoryFactory<Product, typeof User.prototype.id>;
    constructor(dataSource: OrdrinadbDataSource, userCredentialRepositoryGetter: Getter<UserCredentialRepository>, orderRepositoryGetter: Getter<OrderRepository>, paymentRepositoryGetter: Getter<PaymentRepository>, companyRepositoryGetter: Getter<CompanyRepository>, productRepositoryGetter: Getter<ProductRepository>);
    findCredentials(userId: string): Promise<UserCredential>;
}
