import { ApplicationConfig, OrdrinaApplication } from './application';
export * from './application';
export declare function main(options?: ApplicationConfig): Promise<OrdrinaApplication>;
