"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./company-order.controller"), exports);
tslib_1.__exportStar(require("./company-payment.controller"), exports);
tslib_1.__exportStar(require("./company-product.controller"), exports);
tslib_1.__exportStar(require("./customer-order.controller"), exports);
tslib_1.__exportStar(require("./customer-payment.controller"), exports);
tslib_1.__exportStar(require("./delete.controller"), exports);
tslib_1.__exportStar(require("./order-payment.controller"), exports);
tslib_1.__exportStar(require("./ping.controller"), exports);
tslib_1.__exportStar(require("./user-company.controller"), exports);
tslib_1.__exportStar(require("./user.controller"), exports);
tslib_1.__exportStar(require("./product.controller"), exports);
//# sourceMappingURL=index.js.map