/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {authenticate, TokenService} from '@loopback/authentication';
import {
  Credentials,
  MyUserService,
  TokenServiceBindings,
  UserServiceBindings,
} from '@loopback/authentication-jwt';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  patch,
  post,
  requestBody,
  response
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {genSalt, hash} from 'bcryptjs';
import {default as _, default as __} from 'lodash';
import {NewUserRequest, User} from '../models';
import {CompanyRepository, UserRepository} from '../repositories';
import {CredentialsSchema} from '../schemas';


const emailPattern = /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

@authenticate('jwt')
@authorize({allowedRoles: ['admin']})
export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository)
    protected userRepository: UserRepository,
    @repository(CompanyRepository)
    protected companyRepository: CompanyRepository,
  ) {}

  @authenticate.skip()
  @authorize.skip()
  @post('/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(
    @requestBody({
      description: 'The input of login function',
      required: true,
      content: {
        'application/json': {schema: CredentialsSchema},
      },
    }) credentials: Credentials,
  ): Promise<{token: string}> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    return {token};
  }

  @authorize.skip()
  @get('/me', {
    responses: {
      '200': {
        description: 'Return current user',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async whoAmI(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<object> {
    const user = await this.userRepository.findById(currentUserProfile[securityId],{include:['companies']})

    const res = user.toJSON();
    if(user?.companies?.length){
      // @ts-ignore: appending company to /me
      res.company = user.companies[0].toJSON();
    }

    return res;
  }

  @authorize.skip()
  @patch('/me')
  @response(204, {
    description: 'User PATCH success',
  })
  async updateProfile(
    @inject(SecurityBindings.USER) loggedInUser: UserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
      },
    })
    user: object,
  ): Promise<object> {
    /**
     * controller returns back currently logged in user information
     */

    //load the user profile with agency
    const userProfile: User = await this.userRepository.findById(
      loggedInUser.id,
      {
        include: ['companies'],
      },
    );

    // @ts-ignore: Expecting to have add field 'company'
    const company = user?.company;

    //update company details
    if (company) {
      if (company.email && !emailPattern.test(company.email)) {
        throw new HttpErrors.BadRequest('Invalid Email');
      }

      await this.companyRepository.updateById(
        userProfile.companies[0].id,
        company,
      );
    }

    //update user profile
    //if company is added to the object remove it before posting to user profile
    // @ts-ignore: Expecting to have add field 'company'
    delete user?.company;
    //only update if user object is not empty
    if (!__.isEmpty(user)) {
      await this.userRepository.updateById(loggedInUser.id, user);
    }

    //reload updated user profile
    const updatedUser = await this.userRepository.findById(
      loggedInUser.id
    );

    const res: object = updatedUser.toJSON();
    if (userProfile.companies?.length && userProfile.companies[0].id) {
      const com = await this.companyRepository.findById(userProfile.companies[0].id);

      // @ts-ignore: appending company to /me
      res.company = com.toJSON();
    }

    return res;
  }

  @authenticate.skip()
  @authorize.skip()
  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User,
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(NewUserRequest, {
            title: 'NewUser',
            exclude: ['id'],
          }),
        },
      },
    })
    newUserRequest: Omit<NewUserRequest, 'id'>,
  ): Promise<User> {
    const password = await hash(newUserRequest.password, await genSalt());
    const savedUser = await this.userRepository.create(
      _.omit(newUserRequest, 'password'),
    );

    await this.userRepository.userCredentials(savedUser.id).create({password});

    return savedUser;
  }
}
