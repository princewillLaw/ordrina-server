"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampsoftcrud_repository_base_1 = require("./dto/timestampsoftcrud.repository.base");
let CompanyRepository = class CompanyRepository extends timestampsoftcrud_repository_base_1.TimestampSoftCrudRepository {
    constructor(dataSource, productRepositoryGetter, orderRepositoryGetter, paymentRepositoryGetter) {
        super(models_1.Company, dataSource);
        this.productRepositoryGetter = productRepositoryGetter;
        this.orderRepositoryGetter = orderRepositoryGetter;
        this.paymentRepositoryGetter = paymentRepositoryGetter;
        this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter);
        this.registerInclusionResolver('payments', this.payments.inclusionResolver);
        this.orders = this.createHasManyRepositoryFactoryFor('orders', orderRepositoryGetter);
        this.registerInclusionResolver('orders', this.orders.inclusionResolver);
        this.products = this.createHasManyRepositoryFactoryFor('products', productRepositoryGetter);
        this.registerInclusionResolver('products', this.products.inclusionResolver);
    }
};
CompanyRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.ordrinadb')),
    tslib_1.__param(1, repository_1.repository.getter('ProductRepository')),
    tslib_1.__param(2, repository_1.repository.getter('OrderRepository')),
    tslib_1.__param(3, repository_1.repository.getter('PaymentRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.OrdrinadbDataSource, Function, Function, Function])
], CompanyRepository);
exports.CompanyRepository = CompanyRepository;
//# sourceMappingURL=company.repository.js.map