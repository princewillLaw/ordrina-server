import { TokenService } from '@loopback/authentication';
import { Credentials, MyUserService } from '@loopback/authentication-jwt';
import { UserProfile } from '@loopback/security';
import { NewUserRequest, User } from '../models';
import { CompanyRepository, UserRepository } from '../repositories';
export declare class UserController {
    jwtService: TokenService;
    userService: MyUserService;
    user: UserProfile;
    protected userRepository: UserRepository;
    protected companyRepository: CompanyRepository;
    constructor(jwtService: TokenService, userService: MyUserService, user: UserProfile, userRepository: UserRepository, companyRepository: CompanyRepository);
    login(credentials: Credentials): Promise<{
        token: string;
    }>;
    whoAmI(currentUserProfile: UserProfile): Promise<object>;
    updateProfile(loggedInUser: UserProfile, user: object): Promise<object>;
    signUp(newUserRequest: Omit<NewUserRequest, 'id'>): Promise<User>;
}
