"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampsoftcrud_repository_base_1 = require("./dto/timestampsoftcrud.repository.base");
let PaymentRepository = class PaymentRepository extends timestampsoftcrud_repository_base_1.TimestampSoftCrudRepository {
    constructor(dataSource) {
        super(models_1.Payment, dataSource);
    }
};
PaymentRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.ordrinadb')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.OrdrinadbDataSource])
], PaymentRepository);
exports.PaymentRepository = PaymentRepository;
//# sourceMappingURL=payment.repository.js.map