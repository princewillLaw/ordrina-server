import { SoftDeleteEntity } from 'loopback4-soft-delete';
import { USER_ROLE } from '../types/enum';
import { Company } from './company.model';
import { Order } from './order.model';
import { Payment } from './payment.model';
import { Product } from './product.model';
import { UserCredential } from './user-credential.model';
export declare class User extends SoftDeleteEntity {
    id?: string;
    firstName: string;
    lastName: string;
    email: string;
    role: USER_ROLE;
    phone?: string;
    address?: string;
    avatar?: string;
    createdAt?: Date;
    updatedAt?: Date;
    userCredentials: UserCredential;
    orders: Order[];
    payments: Payment[];
    companies: Company[];
    products: Product[];
    constructor(data?: Partial<User>);
}
export interface UserRelations {
}
export type UserWithRelations = User & UserRelations;
