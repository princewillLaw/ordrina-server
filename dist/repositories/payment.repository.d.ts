import { OrdrinadbDataSource } from '../datasources';
import { Payment, PaymentRelations } from '../models';
import { TimestampSoftCrudRepository } from './dto/timestampsoftcrud.repository.base';
export declare class PaymentRepository extends TimestampSoftCrudRepository<Payment, typeof Payment.prototype.id, PaymentRelations> {
    constructor(dataSource: OrdrinadbDataSource);
}
