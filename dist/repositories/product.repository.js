"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampsoftcrud_repository_base_1 = require("./dto/timestampsoftcrud.repository.base");
let ProductRepository = class ProductRepository extends timestampsoftcrud_repository_base_1.TimestampSoftCrudRepository {
    constructor(dataSource) {
        super(models_1.Product, dataSource);
    }
};
ProductRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.ordrinadb')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.OrdrinadbDataSource])
], ProductRepository);
exports.ProductRepository = ProductRepository;
//# sourceMappingURL=product.repository.js.map