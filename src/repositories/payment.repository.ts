import {inject} from '@loopback/core';
import {OrdrinadbDataSource} from '../datasources';
import {Payment, PaymentRelations} from '../models';
import {TimestampSoftCrudRepository} from './dto/timestampsoftcrud.repository.base';

export class PaymentRepository extends TimestampSoftCrudRepository<
  Payment,
  typeof Payment.prototype.id,
  PaymentRelations
> {
  constructor(
    @inject('datasources.ordrinadb') dataSource: OrdrinadbDataSource,
  ) {
    super(Payment, dataSource);
  }
}
