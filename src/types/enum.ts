/* eslint-disable @typescript-eslint/naming-convention */

export enum USER_ROLE {
  admin = 'admin',
  customer = 'customer',
  business_owner = 'business_owner'
}
