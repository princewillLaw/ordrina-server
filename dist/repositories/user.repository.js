"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const datasources_1 = require("../datasources");
const models_1 = require("../models");
const timestampsoftcrud_repository_base_1 = require("./dto/timestampsoftcrud.repository.base");
let UserRepository = class UserRepository extends timestampsoftcrud_repository_base_1.TimestampSoftCrudRepository {
    constructor(dataSource, userCredentialRepositoryGetter, orderRepositoryGetter, paymentRepositoryGetter, companyRepositoryGetter, productRepositoryGetter) {
        super(models_1.User, dataSource);
        this.userCredentialRepositoryGetter = userCredentialRepositoryGetter;
        this.orderRepositoryGetter = orderRepositoryGetter;
        this.paymentRepositoryGetter = paymentRepositoryGetter;
        this.companyRepositoryGetter = companyRepositoryGetter;
        this.productRepositoryGetter = productRepositoryGetter;
        this.products = this.createHasManyRepositoryFactoryFor('products', productRepositoryGetter);
        this.registerInclusionResolver('products', this.products.inclusionResolver);
        this.companies = this.createHasManyRepositoryFactoryFor('companies', companyRepositoryGetter);
        this.registerInclusionResolver('companies', this.companies.inclusionResolver);
        this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter);
        this.registerInclusionResolver('payments', this.payments.inclusionResolver);
        this.orders = this.createHasManyRepositoryFactoryFor('orders', orderRepositoryGetter);
        this.registerInclusionResolver('orders', this.orders.inclusionResolver);
        this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialRepositoryGetter);
        this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
    }
    async findCredentials(userId) {
        return this.userCredentials(userId).get();
    }
};
UserRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)('datasources.ordrinadb')),
    tslib_1.__param(1, repository_1.repository.getter('UserCredentialRepository')),
    tslib_1.__param(2, repository_1.repository.getter('OrderRepository')),
    tslib_1.__param(3, repository_1.repository.getter('PaymentRepository')),
    tslib_1.__param(4, repository_1.repository.getter('CompanyRepository')),
    tslib_1.__param(5, repository_1.repository.getter('ProductRepository')),
    tslib_1.__metadata("design:paramtypes", [datasources_1.OrdrinadbDataSource, Function, Function, Function, Function, Function])
], UserRepository);
exports.UserRepository = UserRepository;
//# sourceMappingURL=user.repository.js.map