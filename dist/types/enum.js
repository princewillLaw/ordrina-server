"use strict";
/* eslint-disable @typescript-eslint/naming-convention */
Object.defineProperty(exports, "__esModule", { value: true });
exports.USER_ROLE = void 0;
var USER_ROLE;
(function (USER_ROLE) {
    USER_ROLE["admin"] = "admin";
    USER_ROLE["customer"] = "customer";
    USER_ROLE["business_owner"] = "business_owner";
})(USER_ROLE = exports.USER_ROLE || (exports.USER_ROLE = {}));
//# sourceMappingURL=enum.js.map