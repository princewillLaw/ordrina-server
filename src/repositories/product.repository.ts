import {inject} from '@loopback/core';
import {OrdrinadbDataSource} from '../datasources';
import {Product, ProductRelations} from '../models';
import {TimestampSoftCrudRepository} from './dto/timestampsoftcrud.repository.base';

export class ProductRepository extends TimestampSoftCrudRepository<
  Product,
  typeof Product.prototype.id,
  ProductRelations
> {

  constructor(
    @inject('datasources.ordrinadb') dataSource: OrdrinadbDataSource,
  ) {
    super(Product, dataSource);
  }
}
