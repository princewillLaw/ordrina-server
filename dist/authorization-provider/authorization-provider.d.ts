/// <reference types="express" />
import { AuthorizationContext, AuthorizationDecision, AuthorizationMetadata, Authorizer } from '@loopback/authorization';
import { Provider } from '@loopback/core';
import { Request } from '@loopback/rest';
import { UserProfile } from '@loopback/security';
import { CompanyRepository, UserRepository } from '../repositories';
export declare class MyAuthorizationProvider implements Provider<Authorizer> {
    private userRepository;
    private req;
    private companyRepository;
    constructor(user: UserProfile, userRepository: UserRepository, req: Request, companyRepository: CompanyRepository);
    /**
     * @returns authenticateFn
     */
    value(): Authorizer;
    authorize(authorizationCtx: AuthorizationContext, metadata: AuthorizationMetadata): Promise<AuthorizationDecision.ALLOW | AuthorizationDecision.DENY>;
}
