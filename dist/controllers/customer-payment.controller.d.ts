import { Count, Filter, Where } from '@loopback/repository';
import { Payment, User } from '../models';
import { UserRepository } from '../repositories';
export declare class CustomerPaymentController {
    protected userRepository: UserRepository;
    constructor(userRepository: UserRepository);
    find(id: string, filter?: Filter<Payment>): Promise<Payment[]>;
    create(id: typeof User.prototype.id, payment: Omit<Payment, 'id'>): Promise<Payment>;
    patch(id: string, payment: Partial<Payment>, where?: Where<Payment>): Promise<Count>;
    delete(id: string, where?: Where<Payment>): Promise<Count>;
}
