import { Getter } from '@loopback/core';
import { HasManyRepositoryFactory } from '@loopback/repository';
import { OrdrinadbDataSource } from '../datasources';
import { Order, OrderRelations, Payment } from '../models';
import { TimestampSoftCrudRepository } from './dto/timestampsoftcrud.repository.base';
import { PaymentRepository } from './payment.repository';
export declare class OrderRepository extends TimestampSoftCrudRepository<Order, typeof Order.prototype.id, OrderRelations> {
    protected paymentRepositoryGetter: Getter<PaymentRepository>;
    readonly payments: HasManyRepositoryFactory<Payment, typeof Order.prototype.id>;
    constructor(dataSource: OrdrinadbDataSource, paymentRepositoryGetter: Getter<PaymentRepository>);
}
