import { Count, Filter, Where } from '@loopback/repository';
import { Company, Product } from '../models';
import { CompanyRepository } from '../repositories';
export declare class CompanyProductController {
    protected companyRepository: CompanyRepository;
    constructor(companyRepository: CompanyRepository);
    find(id: string, filter?: Filter<Product>): Promise<Product[]>;
    create(id: typeof Company.prototype.id, product: Omit<Product, 'id'>): Promise<Product>;
    patch(id: string, product: Partial<Product>, where?: Where<Product>): Promise<Count>;
    delete(id: string, where?: Where<Product>): Promise<Count>;
}
