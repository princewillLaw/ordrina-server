import {inject} from '@loopback/core';
import {DataObject, Entity, Where} from '@loopback/repository';
import {Count} from '@loopback/repository/src/common-types';
import {Options} from 'loopback-datasource-juggler';
import {SoftCrudRepository, SoftDeleteEntity} from 'loopback4-soft-delete';
import {OrdrinadbDataSource} from '../../datasources';

export abstract class TimestampSoftCrudRepository<
  E extends SoftDeleteEntity & {createdAt?: Date; updatedAt?: Date},
  ID,
  Relations extends object = {},
> extends SoftCrudRepository<E, ID, Relations> {
  constructor(
    entityClass: typeof Entity & {
      prototype: E;
    },
    @inject('datasources.mongo') dataSource: OrdrinadbDataSource,
  ) {
    super(entityClass, dataSource);
  }

  async create(entity: DataObject<E>, options?: Options): Promise<E> {
    entity.createdAt = new Date();
    entity.updatedAt = new Date();
    return super.create(entity, options);
  }

  async updateAll(
    data: DataObject<E>,
    where?: Where<E>,
    options?: Options,
  ): Promise<Count> {
    data.updatedAt = new Date();
    return super.updateAll(data, where, options);
  }

  async updateById(
    id: ID,
    data: DataObject<E>,
    options?: Options,
  ): Promise<void> {
    data.updatedAt = new Date();
    return super.updateById(id, data, options);
  }

  async replaceById(
    id: ID,
    data: DataObject<E>,
    options?: Options,
  ): Promise<void> {
    data.updatedAt = new Date();
    return super.replaceById(id, data, options);
  }
}
