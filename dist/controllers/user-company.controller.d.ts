import { Count, Filter, Where } from '@loopback/repository';
import { Company, User } from '../models';
import { UserRepository } from '../repositories';
export declare class UserCompanyController {
    protected userRepository: UserRepository;
    constructor(userRepository: UserRepository);
    find(id: string, filter?: Filter<Company>): Promise<Company[]>;
    create(id: typeof User.prototype.id, company: Omit<Company, 'id'>): Promise<Company>;
    patch(id: string, company: Partial<Company>, where?: Where<Company>): Promise<Count>;
    delete(id: string, where?: Where<Company>): Promise<Count>;
}
