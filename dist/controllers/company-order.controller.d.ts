import { Count, Filter, Where } from '@loopback/repository';
import { Company, Order } from '../models';
import { CompanyRepository } from '../repositories';
export declare class CompanyOrderController {
    protected companyRepository: CompanyRepository;
    constructor(companyRepository: CompanyRepository);
    find(id: string, filter?: Filter<Order>): Promise<Order[]>;
    create(id: typeof Company.prototype.id, order: Omit<Order, 'id'>): Promise<Order>;
    patch(id: string, order: Partial<Order>, where?: Where<Order>): Promise<Count>;
    delete(id: string, where?: Where<Order>): Promise<Count>;
}
