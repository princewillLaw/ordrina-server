import { DataObject, Entity, Where } from '@loopback/repository';
import { Count } from '@loopback/repository/src/common-types';
import { Options } from 'loopback-datasource-juggler';
import { SoftCrudRepository, SoftDeleteEntity } from 'loopback4-soft-delete';
import { OrdrinadbDataSource } from '../../datasources';
export declare abstract class TimestampSoftCrudRepository<E extends SoftDeleteEntity & {
    createdAt?: Date;
    updatedAt?: Date;
}, ID, Relations extends object = {}> extends SoftCrudRepository<E, ID, Relations> {
    constructor(entityClass: typeof Entity & {
        prototype: E;
    }, dataSource: OrdrinadbDataSource);
    create(entity: DataObject<E>, options?: Options): Promise<E>;
    updateAll(data: DataObject<E>, where?: Where<E>, options?: Options): Promise<Count>;
    updateById(id: ID, data: DataObject<E>, options?: Options): Promise<void>;
    replaceById(id: ID, data: DataObject<E>, options?: Options): Promise<void>;
}
