import { SoftDeleteEntity } from 'loopback4-soft-delete';
export declare class Payment extends SoftDeleteEntity {
    id?: string;
    paymentNumber: string;
    amount: number;
    currency: string;
    paymentMethod: string;
    createdAt?: Date;
    updatedAt?: Date;
    companyId?: string;
    customerId?: string;
    orderId?: string;
    constructor(data?: Partial<Payment>);
}
export interface PaymentRelations {
}
export type PaymentWithRelations = Payment & PaymentRelations;
