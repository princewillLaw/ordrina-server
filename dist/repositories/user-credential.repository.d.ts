import { DefaultCrudRepository } from '@loopback/repository';
import { OrdrinadbDataSource } from '../datasources';
import { UserCredential, UserCredentialRelations } from '../models';
export declare class UserCredentialRepository extends DefaultCrudRepository<UserCredential, typeof UserCredential.prototype.id, UserCredentialRelations> {
    constructor(dataSource: OrdrinadbDataSource);
}
