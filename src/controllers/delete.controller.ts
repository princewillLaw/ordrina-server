/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {repository} from '@loopback/repository';
import {
  del,
  param,
  response
} from '@loopback/rest';
import {CompanyRepository, OrderRepository, PaymentRepository, ProductRepository, UserRepository} from '../repositories';

@authenticate('jwt')
@authorize({allowedRoles: ['admin']})
export class DeleteController {
  constructor(
    @repository(UserRepository)
    protected userRepository: UserRepository,
    @repository(CompanyRepository)
    protected companyRepository: CompanyRepository,
    @repository(ProductRepository)
    protected productRepository: ProductRepository,
    @repository(OrderRepository)
    protected orderRepository: OrderRepository,
    @repository(PaymentRepository)
    protected paymentRepository: PaymentRepository,
  ) {}

  // delete a product
  @authorize({allowedRoles: ['admin','$owner']})
  @del('/users/{userId}/products/{productId}')
  @response(204, {
    description: 'Product DELETE success',
  })
  async deleteProductById(
    @param.path.string('userId') userId: string,
    @param.path.string('productId') productId: string,
  ): Promise<void> {
    const products = await  this.userRepository.products(userId).find({where:{id: productId}});

    if(products?.length){
      await this.productRepository.deleteById(productId);
    }
  }

  // delete an order
  @del('/users/{userId}/orders/{orderId}')
  @response(204, {
    description: 'Order DELETE success',
  })
  async deleteOrderById(
    @param.path.string('userId') userId: string,
    @param.path.string('orderId') orderId: string,
  ): Promise<void> {
    const orders = await  this.userRepository.orders(userId).find({where:{id: orderId}});

    if(orders?.length){
      await this.orderRepository.deleteById(orderId);
    }
  }

  // delete a payment
  @del('/users/{userId}/payments/{paymentId}')
  @response(204, {
    description: 'Payment DELETE success',
  })
  async deletePaymentById(
    @param.path.string('userId') userId: string,
    @param.path.string('paymentId') paymentId: string,
  ): Promise<void> {
    const payments = await  this.userRepository.payments(userId).find({where:{id: paymentId}});

    if(payments?.length){
      await this.paymentRepository.deleteById(paymentId);
    }
  }

  // delete a company
  @del('/users/{userId}/companies/{companyId}')
  @response(204, {
    description: 'Company DELETE success',
  })
  async deleteCompanyById(
    @param.path.string('userId') userId: string,
    @param.path.string('companyId') companyId: string,
  ): Promise<void> {
    const companies = await  this.userRepository.companies(userId).find({where:{id: companyId}});

    if(companies?.length){
      await this.companyRepository.deleteById(companyId);
    }
  }
}
