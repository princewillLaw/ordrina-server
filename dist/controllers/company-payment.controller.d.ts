import { Count, Filter, Where } from '@loopback/repository';
import { Company, Payment } from '../models';
import { CompanyRepository } from '../repositories';
export declare class CompanyPaymentController {
    protected companyRepository: CompanyRepository;
    constructor(companyRepository: CompanyRepository);
    find(id: string, filter?: Filter<Payment>): Promise<Payment[]>;
    create(id: typeof Company.prototype.id, payment: Omit<Payment, 'id'>): Promise<Payment>;
    patch(id: string, payment: Partial<Payment>, where?: Where<Payment>): Promise<Count>;
    delete(id: string, where?: Where<Payment>): Promise<Count>;
}
