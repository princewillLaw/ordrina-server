import {hasMany, hasOne, model, property} from '@loopback/repository';
import {SoftDeleteEntity} from 'loopback4-soft-delete';
import {USER_ROLE} from '../types/enum';
import {Company} from './company.model';
import {Order} from './order.model';
import {Payment} from './payment.model';
import {Product} from './product.model';
import {UserCredential} from './user-credential.model';

@model()
export class User extends SoftDeleteEntity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  firstName: string;

  @property({
    type: 'string',
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    jsonSchema: {
      enum: Object.values(USER_ROLE),
    },
    required: false,
    default: USER_ROLE.customer,
  })
  role: USER_ROLE;

  @property({
    type: 'string',
  })
  phone?: string;

  @property({
    type: 'string',
  })
  address?: string;

  @property({
    type: 'string',
  })
  avatar?: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @hasOne(() => UserCredential)
  userCredentials: UserCredential;

  @hasMany(() => Order, {keyTo: 'customerId'})
  orders: Order[];

  @hasMany(() => Payment, {keyTo: 'customerId'})
  payments: Payment[];

  @hasMany(() => Company)
  companies: Company[];

  @hasMany(() => Product)
  products: Product[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
