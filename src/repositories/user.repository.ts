import {Getter, inject} from '@loopback/core';
import {HasManyRepositoryFactory, HasOneRepositoryFactory, repository} from '@loopback/repository';
import {OrdrinadbDataSource} from '../datasources';
import {Company, Order, Payment, Product, User, UserCredential, UserRelations} from '../models';
import {CompanyRepository} from './company.repository';
import {TimestampSoftCrudRepository} from './dto/timestampsoftcrud.repository.base';
import {OrderRepository} from './order.repository';
import {PaymentRepository} from './payment.repository';
import {ProductRepository} from './product.repository';
import {UserCredentialRepository} from './user-credential.repository';

export class UserRepository extends TimestampSoftCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {

  public readonly userCredentials: HasOneRepositoryFactory<UserCredential, typeof User.prototype.id>;

  public readonly orders: HasManyRepositoryFactory<Order, typeof User.prototype.id>;

  public readonly payments: HasManyRepositoryFactory<Payment, typeof User.prototype.id>;

  public readonly companies: HasManyRepositoryFactory<Company, typeof User.prototype.id>;

  public readonly products: HasManyRepositoryFactory<Product, typeof User.prototype.id>;

  constructor(
    @inject('datasources.ordrinadb') dataSource: OrdrinadbDataSource, @repository.getter('UserCredentialRepository') protected userCredentialRepositoryGetter: Getter<UserCredentialRepository>, @repository.getter('OrderRepository') protected orderRepositoryGetter: Getter<OrderRepository>, @repository.getter('PaymentRepository') protected paymentRepositoryGetter: Getter<PaymentRepository>, @repository.getter('CompanyRepository') protected companyRepositoryGetter: Getter<CompanyRepository>, @repository.getter('ProductRepository') protected productRepositoryGetter: Getter<ProductRepository>,
  ) {
    super(User, dataSource);
    this.products = this.createHasManyRepositoryFactoryFor('products', productRepositoryGetter,);
    this.registerInclusionResolver('products', this.products.inclusionResolver);
    this.companies = this.createHasManyRepositoryFactoryFor('companies', companyRepositoryGetter,);
    this.registerInclusionResolver('companies', this.companies.inclusionResolver);
    this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter,);
    this.registerInclusionResolver('payments', this.payments.inclusionResolver);
    this.orders = this.createHasManyRepositoryFactoryFor('orders', orderRepositoryGetter,);
    this.registerInclusionResolver('orders', this.orders.inclusionResolver);
    this.userCredentials = this.createHasOneRepositoryFactoryFor('userCredentials', userCredentialRepositoryGetter,);
    this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
  }

  async findCredentials(userId: string){
    return this.userCredentials(userId).get()
  }
}
