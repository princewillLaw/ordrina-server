"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright RehomeGhana Ltd 2022 2022. All Rights Reserved.
// Node module: rehome-api
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimeStampRepositoryMixin = void 0;
function TimeStampRepositoryMixin(repository) {
    // @ts-ignore extending unconventional repository c
    class MixedRepository extends repository {
        async create(entity, options) {
            entity.createdAt = new Date();
            entity.updatedAt = new Date();
            return super.create(entity, options);
        }
        async updateAll(data, where, options) {
            data.updatedAt = new Date();
            return super.updateAll(data, where, options);
        }
        async updateById(id, data, options) {
            data.updatedAt = new Date();
            return super.updateById(id, data, options);
        }
        async replaceById(id, data, options) {
            data.updatedAt = new Date();
            return super.replaceById(id, data, options);
        }
    }
    return MixedRepository;
}
exports.TimeStampRepositoryMixin = TimeStampRepositoryMixin;
//# sourceMappingURL=TimestampRepositoryMixin.js.map