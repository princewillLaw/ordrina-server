"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderPaymentController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authorization_1 = require("@loopback/authorization");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let OrderPaymentController = class OrderPaymentController {
    constructor(orderRepository) {
        this.orderRepository = orderRepository;
    }
    async find(id, filter) {
        return this.orderRepository.payments(id).find(filter);
    }
    async create(id, payment) {
        return this.orderRepository.payments(id).create(payment);
    }
    async patch(id, payment, where) {
        return this.orderRepository.payments(id).patch(payment, where);
    }
    async delete(id, where) {
        return this.orderRepository.payments(id).delete(where);
    }
};
tslib_1.__decorate([
    (0, rest_1.get)('/orders/{id}/payments', {
        responses: {
            '200': {
                description: 'Array of Order has many Payment',
                content: {
                    'application/json': {
                        schema: { type: 'array', items: (0, rest_1.getModelSchemaRef)(models_1.Payment) },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.query.object('filter')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrderPaymentController.prototype, "find", null);
tslib_1.__decorate([
    (0, rest_1.post)('/orders/{id}/payments', {
        responses: {
            '200': {
                description: 'Order model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Payment) } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Payment, {
                    title: 'NewPaymentInOrder',
                    exclude: ['id'],
                    optional: ['orderId']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrderPaymentController.prototype, "create", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/orders/{id}/payments', {
        responses: {
            '200': {
                description: 'Order.Payment PATCH success count',
                content: { 'application/json': { schema: repository_1.CountSchema } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Payment, { partial: true }),
            },
        },
    })),
    tslib_1.__param(2, rest_1.param.query.object('where', (0, rest_1.getWhereSchemaFor)(models_1.Payment))),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrderPaymentController.prototype, "patch", null);
tslib_1.__decorate([
    (0, rest_1.del)('/orders/{id}/payments', {
        responses: {
            '200': {
                description: 'Order.Payment DELETE success count',
                content: { 'application/json': { schema: repository_1.CountSchema } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.query.object('where', (0, rest_1.getWhereSchemaFor)(models_1.Payment))),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], OrderPaymentController.prototype, "delete", null);
OrderPaymentController = tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, authorization_1.authorize)({ allowedRoles: ['admin'] }),
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.OrderRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.OrderRepository])
], OrderPaymentController);
exports.OrderPaymentController = OrderPaymentController;
//# sourceMappingURL=order-payment.controller.js.map