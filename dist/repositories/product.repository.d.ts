import { OrdrinadbDataSource } from '../datasources';
import { Product, ProductRelations } from '../models';
import { TimestampSoftCrudRepository } from './dto/timestampsoftcrud.repository.base';
export declare class ProductRepository extends TimestampSoftCrudRepository<Product, typeof Product.prototype.id, ProductRelations> {
    constructor(dataSource: OrdrinadbDataSource);
}
