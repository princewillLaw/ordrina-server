import {Getter, inject} from '@loopback/core';
import {HasManyRepositoryFactory, repository} from '@loopback/repository';
import {OrdrinadbDataSource} from '../datasources';
import {Company, CompanyRelations, Order, Payment, Product} from '../models';
import {TimestampSoftCrudRepository} from './dto/timestampsoftcrud.repository.base';
import {OrderRepository} from './order.repository';
import {PaymentRepository} from './payment.repository';
import {ProductRepository} from './product.repository';

export class CompanyRepository extends TimestampSoftCrudRepository<
  Company,
  typeof Company.prototype.id,
  CompanyRelations
> {

  public readonly products: HasManyRepositoryFactory<Product, typeof Company.prototype.id>;

  public readonly orders: HasManyRepositoryFactory<Order, typeof Company.prototype.id>;

  public readonly payments: HasManyRepositoryFactory<Payment, typeof Company.prototype.id>;

  constructor(
    @inject('datasources.ordrinadb') dataSource: OrdrinadbDataSource, @repository.getter('ProductRepository') protected productRepositoryGetter: Getter<ProductRepository>, @repository.getter('OrderRepository') protected orderRepositoryGetter: Getter<OrderRepository>, @repository.getter('PaymentRepository') protected paymentRepositoryGetter: Getter<PaymentRepository>,
  ) {
    super(Company, dataSource);
    this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter,);
    this.registerInclusionResolver('payments', this.payments.inclusionResolver);
    this.orders = this.createHasManyRepositoryFactoryFor('orders', orderRepositoryGetter,);
    this.registerInclusionResolver('orders', this.orders.inclusionResolver);
    this.products = this.createHasManyRepositoryFactoryFor('products', productRepositoryGetter,);
    this.registerInclusionResolver('products', this.products.inclusionResolver);
  }
}
