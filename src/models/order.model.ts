import {hasMany, model, property} from '@loopback/repository';
import {SoftDeleteEntity} from 'loopback4-soft-delete';
import {Payment} from './payment.model';

@model()
export class Order extends SoftDeleteEntity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  orderNumber: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isDelivered: boolean;

  @property({
    type: 'object',
    required: true,
    jsonSchema: {
      type: 'object',
      description: 'Information about the product ordered',
      properties: {
        id: {
          type: 'string',
          description: 'id of product'
        },
        name: {
          type: 'string',
          description: 'name of product at pruchase time'
        },
        quantity: {
          type: 'number',
          description: 'quantity of product purchased'
        },
        unitPrice: {
          type: 'number',
          description: 'price paid for 1 quantity of product'
        },
        costPrice: {
          type: 'number',
          description: 'cost price for 1 quantity of product at purchase time'
        }
      },
      required: ['id', 'name', 'quantity', 'unitPrice', 'costPrice']
    }
  })
  productInformation: object;

  @property({
    type: 'string',
    required: true,
  })
  status: string;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  createdAt?: Date;

  @property({
    type: 'date',
    default: () => new Date(),
  })
  updatedAt?: Date;

  @property({
    type: 'string',
  })
  companyId?: string;

  @property({
    type: 'string',
  })
  customerId?: string;

  @hasMany(() => Payment)
  payments: Payment[];

  constructor(data?: Partial<Order>) {
    super(data);
  }
}

export interface OrderRelations {
  // describe navigational properties here
}

export type OrderWithRelations = Order & OrderRelations;
