export declare enum USER_ROLE {
    admin = "admin",
    customer = "customer",
    business_owner = "business_owner"
}
