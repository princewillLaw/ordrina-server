"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const loopback4_soft_delete_1 = require("loopback4-soft-delete");
const payment_model_1 = require("./payment.model");
let Order = class Order extends loopback4_soft_delete_1.SoftDeleteEntity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        id: true,
        generated: true,
    }),
    tslib_1.__metadata("design:type", String)
], Order.prototype, "id", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Order.prototype, "orderNumber", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'boolean',
        required: true,
    }),
    tslib_1.__metadata("design:type", Boolean)
], Order.prototype, "isDelivered", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'object',
        required: true,
        jsonSchema: {
            type: 'object',
            description: 'Information about the product ordered',
            properties: {
                id: {
                    type: 'string',
                    description: 'id of product'
                },
                name: {
                    type: 'string',
                    description: 'name of product at pruchase time'
                },
                quantity: {
                    type: 'number',
                    description: 'quantity of product purchased'
                },
                unitPrice: {
                    type: 'number',
                    description: 'price paid for 1 quantity of product'
                },
                costPrice: {
                    type: 'number',
                    description: 'cost price for 1 quantity of product at purchase time'
                }
            },
            required: ['id', 'name', 'quantity', 'unitPrice', 'costPrice']
        }
    }),
    tslib_1.__metadata("design:type", Object)
], Order.prototype, "productInformation", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Order.prototype, "status", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date',
        default: () => new Date(),
    }),
    tslib_1.__metadata("design:type", Date)
], Order.prototype, "createdAt", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'date',
        default: () => new Date(),
    }),
    tslib_1.__metadata("design:type", Date)
], Order.prototype, "updatedAt", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], Order.prototype, "companyId", void 0);
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], Order.prototype, "customerId", void 0);
tslib_1.__decorate([
    (0, repository_1.hasMany)(() => payment_model_1.Payment),
    tslib_1.__metadata("design:type", Array)
], Order.prototype, "payments", void 0);
Order = tslib_1.__decorate([
    (0, repository_1.model)(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Order);
exports.Order = Order;
//# sourceMappingURL=order.model.js.map