import { Count, Filter, Where } from '@loopback/repository';
import { Order, User } from '../models';
import { UserRepository } from '../repositories';
export declare class CustomerOrderController {
    protected userRepository: UserRepository;
    constructor(userRepository: UserRepository);
    find(id: string, filter?: Filter<Order>): Promise<Order[]>;
    create(id: typeof User.prototype.id, order: Omit<Order, 'id'>): Promise<Order>;
    patch(id: string, order: Partial<Order>, where?: Where<Order>): Promise<Count>;
    delete(id: string, where?: Where<Order>): Promise<Count>;
}
