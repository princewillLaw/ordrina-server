import { Constructor, Entity, EntityCrudRepository } from '@loopback/repository';
export declare function TimeStampRepositoryMixin<E extends Entity & {
    createdAt?: Date;
    updatedAt?: Date;
}, ID, R extends Constructor<EntityCrudRepository<E, ID>>>(repository: R): unknown;
