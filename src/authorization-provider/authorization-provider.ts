/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright RehomeGhana Ltd 2022 2022. All Rights Reserved.
// Node module: rehome-api
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  AuthorizationContext,
  AuthorizationDecision,
  AuthorizationMetadata,
  Authorizer,
} from '@loopback/authorization';
import {Provider, inject} from '@loopback/core';
import {repository} from '@loopback/repository';
import {Request, RestBindings} from '@loopback/rest';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {User} from '../models';
import {
  CompanyRepository,
  UserRepository,
} from '../repositories';

export class MyAuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @inject(SecurityBindings.USER) user: UserProfile,
    @repository(UserRepository)
    private userRepository: UserRepository,
    @inject(RestBindings.Http.REQUEST)
    private req: Request,
    @repository(CompanyRepository)
    private companyRepository: CompanyRepository,
  ) {}

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    for (const arg of authorizationCtx.invocationContext.args) {
      if (
        typeof arg === 'object' &&
        JSON.stringify(arg).includes('credentials')
      ) {
        return AuthorizationDecision.DENY;
      }
    }
    let currentUser: User;

    if (authorizationCtx.principals.length > 0) {
      currentUser = await this.userRepository.findById(
        authorizationCtx.principals[0].id
      );
    } else {
      return AuthorizationDecision.DENY;
    }

    // Deny if no user was gotten
    if (!currentUser) {
      return AuthorizationDecision.DENY;
    }

    // Authorize everything that does not have a allowedRoles property
    if (!metadata.allowedRoles) {
      return AuthorizationDecision.ALLOW;
    }

    // check if userType or subscribed is allowed to access the data
    const roles = ['' + currentUser.role];

    // Admin accounts bypass id verification
    if (roles.includes('admin')) {
      return AuthorizationDecision.ALLOW;
    }

    // check if he/she is owner of content
    if (metadata.allowedRoles.includes('$owner')) {
      // check if the endpoint first args is string representing userId
      // e.g. @get('user/{userId}/XXXXXXX') returns `userId` as args[0]
      if (
        currentUser.id?.toString() ===
        authorizationCtx.invocationContext.args[0].toString()
      ) {
        return AuthorizationDecision.ALLOW;
      }

      /**
       * Allow access to Conversations based resources if user is the owner of the conversation
       * eg. @post('/conversations/{conversationId}/XXXXX', ...) returns `conversationId` as args[0]
       */
      try {
        if (
          // @ts-ignore: ignore if the target object is missing
          authorizationCtx.invocationContext.target?.companyRepository &&
          // @ts-ignore: ignore if the target object is missing
          authorizationCtx.invocationContext.target?.companyRepository
            ?.modelClass.name === 'Company'
        ) {
          //let's make sure that the user is the owner of the conversation they are trying to access
          // @ts-ignore: ignore if the target object is missing
          const companyId = authorizationCtx.invocationContext.args[0];
          const company = await this.companyRepository.findById(
            companyId,
          );
          //confirm that the conversation belongs to the user
          if (company?.userId === currentUser.id?.toString()) {
            return AuthorizationDecision.ALLOW;
          }
        }
      } catch {
        console.log();
      }
    }

    let roleIsAllowed = false;
    for (const role of roles) {
      if (metadata.allowedRoles!.includes(role)) {
        roleIsAllowed = true;
        break;
      }
    }

    if (roleIsAllowed) {
      return AuthorizationDecision.ALLOW;
    }

    return AuthorizationDecision.DENY;
  }
}
