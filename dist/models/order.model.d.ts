import { SoftDeleteEntity } from 'loopback4-soft-delete';
import { Payment } from './payment.model';
export declare class Order extends SoftDeleteEntity {
    id?: string;
    orderNumber: string;
    isDelivered: boolean;
    productInformation: object;
    status: string;
    createdAt?: Date;
    updatedAt?: Date;
    companyId?: string;
    customerId?: string;
    payments: Payment[];
    constructor(data?: Partial<Order>);
}
export interface OrderRelations {
}
export type OrderWithRelations = Order & OrderRelations;
