"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyOrderController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authorization_1 = require("@loopback/authorization");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let CompanyOrderController = class CompanyOrderController {
    constructor(companyRepository) {
        this.companyRepository = companyRepository;
    }
    async find(id, filter) {
        return this.companyRepository.orders(id).find(filter);
    }
    async create(id, order) {
        return this.companyRepository.orders(id).create(order);
    }
    async patch(id, order, where) {
        return this.companyRepository.orders(id).patch(order, where);
    }
    async delete(id, where) {
        return this.companyRepository.orders(id).delete(where);
    }
};
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.get)('/companies/{id}/orders', {
        responses: {
            '200': {
                description: 'Array of Company has many Order',
                content: {
                    'application/json': {
                        schema: { type: 'array', items: (0, rest_1.getModelSchemaRef)(models_1.Order) },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.query.object('filter')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], CompanyOrderController.prototype, "find", null);
tslib_1.__decorate([
    (0, rest_1.post)('/companies/{id}/orders', {
        responses: {
            '200': {
                description: 'Company model instance',
                content: { 'application/json': { schema: (0, rest_1.getModelSchemaRef)(models_1.Order) } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Order, {
                    title: 'NewOrderInCompany',
                    exclude: ['id'],
                    optional: ['companyId']
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], CompanyOrderController.prototype, "create", null);
tslib_1.__decorate([
    (0, rest_1.patch)('/companies/{id}/orders', {
        responses: {
            '200': {
                description: 'Company.Order PATCH success count',
                content: { 'application/json': { schema: repository_1.CountSchema } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.Order, { partial: true }),
            },
        },
    })),
    tslib_1.__param(2, rest_1.param.query.object('where', (0, rest_1.getWhereSchemaFor)(models_1.Order))),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], CompanyOrderController.prototype, "patch", null);
tslib_1.__decorate([
    (0, rest_1.del)('/companies/{id}/orders', {
        responses: {
            '200': {
                description: 'Company.Order DELETE success count',
                content: { 'application/json': { schema: repository_1.CountSchema } },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__param(1, rest_1.param.query.object('where', (0, rest_1.getWhereSchemaFor)(models_1.Order))),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], CompanyOrderController.prototype, "delete", null);
CompanyOrderController = tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, authorization_1.authorize)({ allowedRoles: ['admin'] }),
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.CompanyRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.CompanyRepository])
], CompanyOrderController);
exports.CompanyOrderController = CompanyOrderController;
//# sourceMappingURL=company-order.controller.js.map