import { Count, Filter, Where } from '@loopback/repository';
import { Order, Payment } from '../models';
import { OrderRepository } from '../repositories';
export declare class OrderPaymentController {
    protected orderRepository: OrderRepository;
    constructor(orderRepository: OrderRepository);
    find(id: string, filter?: Filter<Payment>): Promise<Payment[]>;
    create(id: typeof Order.prototype.id, payment: Omit<Payment, 'id'>): Promise<Payment>;
    patch(id: string, payment: Partial<Payment>, where?: Where<Payment>): Promise<Count>;
    delete(id: string, where?: Where<Payment>): Promise<Count>;
}
