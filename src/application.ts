import {BootMixin} from '@loopback/boot';
import {ApplicationConfig} from '@loopback/core';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {
  RestExplorerBindings,
  RestExplorerComponent,
} from '@loopback/rest-explorer';
import {ServiceMixin} from '@loopback/service-proxy';
import path from 'path';
import {MySequence} from './sequence';

import {AuthenticationComponent} from '@loopback/authentication';
import {
  JWTAuthenticationComponent,
  MyUserService,
  TokenServiceBindings,
  UserServiceBindings,
} from '@loopback/authentication-jwt';
import {OrdrinadbDataSource} from './datasources';
import {UserCredentialRepository, UserRepository} from './repositories';

import {
  AuthorizationComponent,
  AuthorizationTags,
} from '@loopback/authorization';
import {MyAuthorizationProvider} from './authorization-provider/authorization-provider';

export {ApplicationConfig};

export class OrdrinaApplication extends BootMixin(
  ServiceMixin(RepositoryMixin(RestApplication)),
) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    // Set up the custom sequence
    this.sequence(MySequence);

    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    // Customize @loopback/rest-explorer configuration here
    this.configure(RestExplorerBindings.COMPONENT).to({
      path: '/explorer',
    });
    this.component(RestExplorerComponent);
    this.component(AuthenticationComponent);
    this.component(JWTAuthenticationComponent);

    // ------ ADD SNIPPET AT THE BOTTOM ---------
    // Mount authentication system
    this.component(AuthenticationComponent);
    // Mount jwt component
    this.component(JWTAuthenticationComponent);
    // Bind datasource
    this.dataSource(OrdrinadbDataSource, UserServiceBindings.DATASOURCE_NAME);
    // ------------- END OF SNIPPET -------------

    //new
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);
    this.bind(UserServiceBindings.USER_CREDENTIALS_REPOSITORY).toClass(
      UserCredentialRepository,
    );
    this.bind(UserServiceBindings.USER_REPOSITORY).toClass(
      UserRepository,
    );

    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      <string>process.env.JWT_TOKEN_SECRET,
    );
    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      <string>process.env.JWT_TOKEN_EXPIRES_IN,
    );

    //authorizations setup
    this.component(AuthorizationComponent);

    // bind the authorizer provider
    this.bind('authorizationProviders.my-authorizer-provider')
    .toProvider(MyAuthorizationProvider)
    .tag(AuthorizationTags.AUTHORIZER);

    this.projectRoot = __dirname;
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true,
      },
    };
  }
}
