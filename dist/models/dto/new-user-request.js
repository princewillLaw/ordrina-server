"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewUserRequest = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const user_model_1 = require("../user.model");
let NewUserRequest = class NewUserRequest extends user_model_1.User {
};
tslib_1.__decorate([
    (0, repository_1.property)({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], NewUserRequest.prototype, "password", void 0);
NewUserRequest = tslib_1.__decorate([
    (0, repository_1.model)()
], NewUserRequest);
exports.NewUserRequest = NewUserRequest;
//# sourceMappingURL=new-user-request.js.map