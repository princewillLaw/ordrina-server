import { Getter } from '@loopback/core';
import { HasManyRepositoryFactory } from '@loopback/repository';
import { OrdrinadbDataSource } from '../datasources';
import { Company, CompanyRelations, Order, Payment, Product } from '../models';
import { TimestampSoftCrudRepository } from './dto/timestampsoftcrud.repository.base';
import { OrderRepository } from './order.repository';
import { PaymentRepository } from './payment.repository';
import { ProductRepository } from './product.repository';
export declare class CompanyRepository extends TimestampSoftCrudRepository<Company, typeof Company.prototype.id, CompanyRelations> {
    protected productRepositoryGetter: Getter<ProductRepository>;
    protected orderRepositoryGetter: Getter<OrderRepository>;
    protected paymentRepositoryGetter: Getter<PaymentRepository>;
    readonly products: HasManyRepositoryFactory<Product, typeof Company.prototype.id>;
    readonly orders: HasManyRepositoryFactory<Order, typeof Company.prototype.id>;
    readonly payments: HasManyRepositoryFactory<Payment, typeof Company.prototype.id>;
    constructor(dataSource: OrdrinadbDataSource, productRepositoryGetter: Getter<ProductRepository>, orderRepositoryGetter: Getter<OrderRepository>, paymentRepositoryGetter: Getter<PaymentRepository>);
}
