import {Getter, inject} from '@loopback/core';
import {HasManyRepositoryFactory, repository} from '@loopback/repository';
import {OrdrinadbDataSource} from '../datasources';
import {Order, OrderRelations, Payment} from '../models';
import {TimestampSoftCrudRepository} from './dto/timestampsoftcrud.repository.base';
import {PaymentRepository} from './payment.repository';

export class OrderRepository extends TimestampSoftCrudRepository<
  Order,
  typeof Order.prototype.id,
  OrderRelations
> {

  public readonly payments: HasManyRepositoryFactory<Payment, typeof Order.prototype.id>;

  constructor(
    @inject('datasources.ordrinadb') dataSource: OrdrinadbDataSource, @repository.getter('PaymentRepository') protected paymentRepositoryGetter: Getter<PaymentRepository>,
  ) {
    super(Order, dataSource);
    this.payments = this.createHasManyRepositoryFactoryFor('payments', paymentRepositoryGetter,);
    this.registerInclusionResolver('payments', this.payments.inclusionResolver);
  }
}
