import { SoftDeleteEntity } from 'loopback4-soft-delete';
import { Order } from './order.model';
import { Payment } from './payment.model';
import { Product } from './product.model';
export declare class Company extends SoftDeleteEntity {
    id?: string;
    name: string;
    description?: string;
    email: string;
    phone?: string;
    address?: string;
    industry: string;
    logo?: string;
    createdAt?: Date;
    updatedAt?: Date;
    products: Product[];
    orders: Order[];
    payments: Payment[];
    userId?: string;
    constructor(data?: Partial<Company>);
}
export interface CompanyRelations {
}
export type CompanyWithRelations = Company & CompanyRelations;
