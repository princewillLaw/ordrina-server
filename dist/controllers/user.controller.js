"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const authorization_1 = require("@loopback/authorization");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const security_1 = require("@loopback/security");
const bcryptjs_1 = require("bcryptjs");
const lodash_1 = tslib_1.__importDefault(require("lodash"));
const models_1 = require("../models");
const repositories_1 = require("../repositories");
const schemas_1 = require("../schemas");
const emailPattern = /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
let UserController = class UserController {
    constructor(jwtService, userService, user, userRepository, companyRepository) {
        this.jwtService = jwtService;
        this.userService = userService;
        this.user = user;
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
    }
    async login(credentials) {
        // ensure the user exists, and the password is correct
        const user = await this.userService.verifyCredentials(credentials);
        // convert a User object into a UserProfile object (reduced set of properties)
        const userProfile = this.userService.convertToUserProfile(user);
        // create a JSON Web Token based on the user profile
        const token = await this.jwtService.generateToken(userProfile);
        return { token };
    }
    async whoAmI(currentUserProfile) {
        var _a;
        const user = await this.userRepository.findById(currentUserProfile[security_1.securityId], { include: ['companies'] });
        const res = user.toJSON();
        if ((_a = user === null || user === void 0 ? void 0 : user.companies) === null || _a === void 0 ? void 0 : _a.length) {
            // @ts-ignore: appending company to /me
            res.company = user.companies[0].toJSON();
        }
        return res;
    }
    async updateProfile(loggedInUser, user) {
        /**
         * controller returns back currently logged in user information
         */
        var _a;
        //load the user profile with agency
        const userProfile = await this.userRepository.findById(loggedInUser.id, {
            include: ['companies'],
        });
        // @ts-ignore: Expecting to have add field 'company'
        const company = user === null || user === void 0 ? void 0 : user.company;
        //update company details
        if (company) {
            if (company.email && !emailPattern.test(company.email)) {
                throw new rest_1.HttpErrors.BadRequest('Invalid Email');
            }
            await this.companyRepository.updateById(userProfile.companies[0].id, company);
        }
        //update user profile
        //if company is added to the object remove it before posting to user profile
        // @ts-ignore: Expecting to have add field 'company'
        user === null || user === void 0 ? true : delete user.company;
        //only update if user object is not empty
        if (!lodash_1.default.isEmpty(user)) {
            await this.userRepository.updateById(loggedInUser.id, user);
        }
        //reload updated user profile
        const updatedUser = await this.userRepository.findById(loggedInUser.id);
        const res = updatedUser.toJSON();
        if (((_a = userProfile.companies) === null || _a === void 0 ? void 0 : _a.length) && userProfile.companies[0].id) {
            const com = await this.companyRepository.findById(userProfile.companies[0].id);
            // @ts-ignore: appending company to /me
            res.company = com.toJSON();
        }
        return res;
    }
    async signUp(newUserRequest) {
        const password = await (0, bcryptjs_1.hash)(newUserRequest.password, await (0, bcryptjs_1.genSalt)());
        const savedUser = await this.userRepository.create(lodash_1.default.omit(newUserRequest, 'password'));
        await this.userRepository.userCredentials(savedUser.id).create({ password });
        return savedUser;
    }
};
tslib_1.__decorate([
    authentication_1.authenticate.skip(),
    authorization_1.authorize.skip(),
    (0, rest_1.post)('/login', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                token: {
                                    type: 'string',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        description: 'The input of login function',
        required: true,
        content: {
            'application/json': { schema: schemas_1.CredentialsSchema },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
tslib_1.__decorate([
    authorization_1.authorize.skip(),
    (0, rest_1.get)('/me', {
        responses: {
            '200': {
                description: 'Return current user',
                content: {
                    'application/json': {
                        schema: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, (0, core_1.inject)(security_1.SecurityBindings.USER)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "whoAmI", null);
tslib_1.__decorate([
    authorization_1.authorize.skip(),
    (0, rest_1.patch)('/me'),
    (0, rest_1.response)(204, {
        description: 'User PATCH success',
    }),
    tslib_1.__param(0, (0, core_1.inject)(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: { type: 'object' },
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "updateProfile", null);
tslib_1.__decorate([
    authentication_1.authenticate.skip(),
    authorization_1.authorize.skip(),
    (0, rest_1.post)('/signup', {
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            'x-ts-type': models_1.User,
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, (0, rest_1.requestBody)({
        content: {
            'application/json': {
                schema: (0, rest_1.getModelSchemaRef)(models_1.NewUserRequest, {
                    title: 'NewUser',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "signUp", null);
UserController = tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, authorization_1.authorize)({ allowedRoles: ['admin'] }),
    tslib_1.__param(0, (0, core_1.inject)(authentication_jwt_1.TokenServiceBindings.TOKEN_SERVICE)),
    tslib_1.__param(1, (0, core_1.inject)(authentication_jwt_1.UserServiceBindings.USER_SERVICE)),
    tslib_1.__param(2, (0, core_1.inject)(security_1.SecurityBindings.USER, { optional: true })),
    tslib_1.__param(3, (0, repository_1.repository)(repositories_1.UserRepository)),
    tslib_1.__param(4, (0, repository_1.repository)(repositories_1.CompanyRepository)),
    tslib_1.__metadata("design:paramtypes", [Object, authentication_jwt_1.MyUserService, Object, repositories_1.UserRepository,
        repositories_1.CompanyRepository])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map