"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TimestampSoftCrudRepository = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const loopback4_soft_delete_1 = require("loopback4-soft-delete");
const datasources_1 = require("../../datasources");
let TimestampSoftCrudRepository = class TimestampSoftCrudRepository extends loopback4_soft_delete_1.SoftCrudRepository {
    constructor(entityClass, dataSource) {
        super(entityClass, dataSource);
    }
    async create(entity, options) {
        entity.createdAt = new Date();
        entity.updatedAt = new Date();
        return super.create(entity, options);
    }
    async updateAll(data, where, options) {
        data.updatedAt = new Date();
        return super.updateAll(data, where, options);
    }
    async updateById(id, data, options) {
        data.updatedAt = new Date();
        return super.updateById(id, data, options);
    }
    async replaceById(id, data, options) {
        data.updatedAt = new Date();
        return super.replaceById(id, data, options);
    }
};
TimestampSoftCrudRepository = tslib_1.__decorate([
    tslib_1.__param(1, (0, core_1.inject)('datasources.mongo')),
    tslib_1.__metadata("design:paramtypes", [Object, datasources_1.OrdrinadbDataSource])
], TimestampSoftCrudRepository);
exports.TimestampSoftCrudRepository = TimestampSoftCrudRepository;
//# sourceMappingURL=timestampsoftcrud.repository.base.js.map