import { Count, Filter, Where } from '@loopback/repository';
import { Product, User } from '../models';
import { UserRepository } from '../repositories';
export declare class UserProductController {
    protected userRepository: UserRepository;
    constructor(userRepository: UserRepository);
    find(id: string, filter?: Filter<Product>): Promise<Product[]>;
    create(id: typeof User.prototype.id, product: Omit<Product, 'id'>): Promise<Product>;
    patch(id: string, product: Partial<Product>, where?: Where<Product>): Promise<Count>;
    delete(id: string, where?: Where<Product>): Promise<Count>;
}
