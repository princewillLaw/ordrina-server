"use strict";
/* eslint-disable @typescript-eslint/ban-ts-comment */
// Copyright IBM Corp. and LoopBack contributors 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const authorization_1 = require("@loopback/authorization");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const repositories_1 = require("../repositories");
let DeleteController = class DeleteController {
    constructor(userRepository, companyRepository, productRepository, orderRepository, paymentRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.paymentRepository = paymentRepository;
    }
    // delete a product
    async deleteProductById(userId, productId) {
        const products = await this.userRepository.products(userId).find({ where: { id: productId } });
        if (products === null || products === void 0 ? void 0 : products.length) {
            await this.productRepository.deleteById(productId);
        }
    }
    // delete an order
    async deleteOrderById(userId, orderId) {
        const orders = await this.userRepository.orders(userId).find({ where: { id: orderId } });
        if (orders === null || orders === void 0 ? void 0 : orders.length) {
            await this.orderRepository.deleteById(orderId);
        }
    }
    // delete a payment
    async deletePaymentById(userId, paymentId) {
        const payments = await this.userRepository.payments(userId).find({ where: { id: paymentId } });
        if (payments === null || payments === void 0 ? void 0 : payments.length) {
            await this.paymentRepository.deleteById(paymentId);
        }
    }
    // delete a company
    async deleteCompanyById(userId, companyId) {
        const companies = await this.userRepository.companies(userId).find({ where: { id: companyId } });
        if (companies === null || companies === void 0 ? void 0 : companies.length) {
            await this.companyRepository.deleteById(companyId);
        }
    }
};
tslib_1.__decorate([
    (0, authorization_1.authorize)({ allowedRoles: ['admin', '$owner'] }),
    (0, rest_1.del)('/users/{userId}/products/{productId}'),
    (0, rest_1.response)(204, {
        description: 'Product DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__param(1, rest_1.param.path.string('productId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], DeleteController.prototype, "deleteProductById", null);
tslib_1.__decorate([
    (0, rest_1.del)('/users/{userId}/orders/{orderId}'),
    (0, rest_1.response)(204, {
        description: 'Order DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__param(1, rest_1.param.path.string('orderId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], DeleteController.prototype, "deleteOrderById", null);
tslib_1.__decorate([
    (0, rest_1.del)('/users/{userId}/payments/{paymentId}'),
    (0, rest_1.response)(204, {
        description: 'Payment DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__param(1, rest_1.param.path.string('paymentId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], DeleteController.prototype, "deletePaymentById", null);
tslib_1.__decorate([
    (0, rest_1.del)('/users/{userId}/companies/{companyId}'),
    (0, rest_1.response)(204, {
        description: 'Company DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('userId')),
    tslib_1.__param(1, rest_1.param.path.string('companyId')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String]),
    tslib_1.__metadata("design:returntype", Promise)
], DeleteController.prototype, "deleteCompanyById", null);
DeleteController = tslib_1.__decorate([
    (0, authentication_1.authenticate)('jwt'),
    (0, authorization_1.authorize)({ allowedRoles: ['admin'] }),
    tslib_1.__param(0, (0, repository_1.repository)(repositories_1.UserRepository)),
    tslib_1.__param(1, (0, repository_1.repository)(repositories_1.CompanyRepository)),
    tslib_1.__param(2, (0, repository_1.repository)(repositories_1.ProductRepository)),
    tslib_1.__param(3, (0, repository_1.repository)(repositories_1.OrderRepository)),
    tslib_1.__param(4, (0, repository_1.repository)(repositories_1.PaymentRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.UserRepository,
        repositories_1.CompanyRepository,
        repositories_1.ProductRepository,
        repositories_1.OrderRepository,
        repositories_1.PaymentRepository])
], DeleteController);
exports.DeleteController = DeleteController;
//# sourceMappingURL=delete.controller.js.map