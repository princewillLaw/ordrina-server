import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Company,
  User,
} from '../models';
import {UserRepository} from '../repositories';

@authenticate('jwt')
@authorize({allowedRoles: ['admin']})
export class UserCompanyController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @authorize({allowedRoles: ['admin','$owner']})
  @get('/users/{id}/companies', {
    responses: {
      '200': {
        description: 'Array of User has many Company',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Company)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Company>,
  ): Promise<Company[]> {
    return this.userRepository.companies(id).find(filter);
  }

  @authorize({allowedRoles: ['admin','$owner']})
  @post('/users/{id}/companies', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Company)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {
            title: 'NewCompanyInUser',
            exclude: ['id'],
            optional: ['userId']
          }),
        },
      },
    }) company: Omit<Company, 'id'>,
  ): Promise<Company> {
    return this.userRepository.companies(id).create(company);
  }

  @patch('/users/{id}/companies', {
    responses: {
      '200': {
        description: 'User.Company PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Company, {partial: true}),
        },
      },
    })
    company: Partial<Company>,
    @param.query.object('where', getWhereSchemaFor(Company)) where?: Where<Company>,
  ): Promise<Count> {
    return this.userRepository.companies(id).patch(company, where);
  }

  @del('/users/{id}/companies', {
    responses: {
      '200': {
        description: 'User.Company DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Company)) where?: Where<Company>,
  ): Promise<Count> {
    return this.userRepository.companies(id).delete(where);
  }
}
