"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdrinadbDataSource = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
// const config = {
//   name: 'ordrinadb',
//   connector: 'mongodb',
//   url: 'mongodb+srv://node-shop:node-shop@node-res-shop-exiyc.mongodb.net/ordrina',
//   host: 'node-res-shop-exiyc.mongodb.net',
//   port: 0,
//   user: 'node-shop',
//   password: 'node-shop',
//   database: 'ordrina',
//   useNewUrlParser: true
// };
const config = {
    name: 'ordrinadb',
    connector: 'mongodb',
    url: process.env.MONGODB_URL,
    host: process.env.MONGODB_HOST,
    port: process.env.MONGODB_PORT,
    user: process.env.MONGODB_USER,
    password: process.env.MONGODB_PASSWORD,
    database: process.env.MONGODB_NAME,
    useNewUrlParser: true
};
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
let OrdrinadbDataSource = class OrdrinadbDataSource extends repository_1.juggler.DataSource {
    constructor(dsConfig = config) {
        super(dsConfig);
    }
};
OrdrinadbDataSource.dataSourceName = 'ordrinadb';
OrdrinadbDataSource.defaultConfig = config;
OrdrinadbDataSource = tslib_1.__decorate([
    (0, core_1.lifeCycleObserver)('datasource'),
    tslib_1.__param(0, (0, core_1.inject)('datasources.config.ordrinadb', { optional: true })),
    tslib_1.__metadata("design:paramtypes", [Object])
], OrdrinadbDataSource);
exports.OrdrinadbDataSource = OrdrinadbDataSource;
//# sourceMappingURL=ordrinadb.datasource.js.map