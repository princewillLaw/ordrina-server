import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Payment,
  User,
} from '../models';
import {UserRepository} from '../repositories';

@authenticate('jwt')
@authorize({allowedRoles: ['admin']})
export class CustomerPaymentController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) { }

  @authorize({allowedRoles: ['admin','$owner']})
  @get('/customers/{id}/payments', {
    responses: {
      '200': {
        description: 'Array of User has many Payment',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Payment)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Payment>,
  ): Promise<Payment[]> {
    return this.userRepository.payments(id).find(filter);
  }

  @authorize({allowedRoles: ['admin','$owner']})
  @post('/customers/{id}/payments', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(Payment)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, {
            title: 'NewPaymentInUser',
            exclude: ['id'],
            optional: ['customerId']
          }),
        },
      },
    }) payment: Omit<Payment, 'id'>,
  ): Promise<Payment> {
    return this.userRepository.payments(id).create(payment);
  }

  @patch('/customers/{id}/payments', {
    responses: {
      '200': {
        description: 'User.Payment PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Payment, {partial: true}),
        },
      },
    })
    payment: Partial<Payment>,
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.userRepository.payments(id).patch(payment, where);
  }

  @del('/customers/{id}/payments', {
    responses: {
      '200': {
        description: 'User.Payment DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Payment)) where?: Where<Payment>,
  ): Promise<Count> {
    return this.userRepository.payments(id).delete(where);
  }
}
