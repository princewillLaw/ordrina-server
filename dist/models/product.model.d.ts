import { SoftDeleteEntity } from 'loopback4-soft-delete';
export declare class Product extends SoftDeleteEntity {
    id?: string;
    name: string;
    quantity: number;
    description: string;
    price: number;
    currency: string;
    costPrice: number;
    createdAt?: Date;
    updatedAt?: Date;
    companyId?: string;
    userId?: string;
    constructor(data?: Partial<Product>);
}
export interface ProductRelations {
}
export type ProductWithRelations = Product & ProductRelations;
